var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var LibroSchema = new Schema({
    nombre: {type: String, required: true},
    descripcion: {type: String, required: true},
    editorial: {type: String, required: true},
    file: {type: Object, required: true},
    fechaSubida: {type: Date, required: true}
});

module.exports = mongoose.model('Libro', LibroSchema);