var Libro = require("../models/libro");

exports.getAllLibros = async () => {
    try {
        return await Libro.find(function(err, data) {
            return data;
        });
    } catch (error) {
        console.error(`Error getting all "Libros" ${error}`);
    }
};

exports.save = async (req) => {
    try {
        var nuevoLibro = new Libro({
            nombre: req.body.nombre,
            descripcion: req.body.descripcion,
            editorial: req.body.editorial,
            file: req.file,
            fechaSubida: Date.now()
        });
        nuevoLibro.save();
        return nuevoLibro;
    } catch (error) {
        console.error(`Error saving nuevoLibro ${error}`);
    }
};