var express = require("express");
var path = require("path");
var router = express.Router();
var controllerDir = "../controllers";
var librosController = require(path.join(controllerDir, "libros"));
//var uploadController = require(path.join(controllerDir, "upload"));

// Importar recursos para subir archivos
var multer = require('multer');

// Variable con función DiskStorage para controlar la subida
var storage = multer.diskStorage({   
  destination: function(req, file, cb) { 
     cb(null, './uploads');    
  }, 
  filename: function (req, file, cb) { 
     cb(null , file.originalname);   
  }
});

// Variable para importar multer con la función de DiskStorage
var upload = multer({ storage: storage }).single("archivo_libro");

router.get("/books/new", (req, res, next) => {
    res.render("books-new.pug");
});

router.post("/books/create", async (req, res, next) => {
    upload(req, res, (err) => {
        if(err) {
          res.status(400).send("Error subiendo archivo");
        }
        next();
    });
});

router.post("/books/create", async (req, res, next) => {
    //uploadController.uploadFile(req, res);
    /*El req.file lo devuelve el uploadFile y contiene
    información que se puede usar en el controlador para
    crear el nuevo libro en la base de datos*/
    var nuevoLibro = await librosController.save(req);
    res.redirect("/books/list");
});

router.get("/books/list", async (req, res, next) => {
    var libros = await librosController.getAllLibros();
    var numLibros = libros.length;
    res.render("books-list.pug", {libros: libros, numLibros: numLibros});
});

module.exports = router;