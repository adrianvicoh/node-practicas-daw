var express = require("express");
var path = require("path");
var router = express.Router();
var controllerDir = "../controllers";
var alumnosController = require(path.join(controllerDir, "alumnos"));
var asignaturasController = require(path.join(controllerDir, "asignaturas"));

router.get("/", async (req, res, next) => {
    res.send("<h1>Hello World</h1>Paso1: Visita la pagina /alumnos/list<br>Paso 2: Visita la pagina /alumnos/show?nombre=Juan");
});

router.get("/alumnos/list", async (req, res, next) => {
    var alumnos = await alumnosController.getAllAlumnos();
    console.log(alumnos);
    res.render("alumnos-list.pug", {alumnos: alumnos});
});

router.get("/alumnos/show", async (req, res, next) => {
    var newAlumno = await alumnosController.showAlumno(req);
    res.render("alumnos-show.pug", {alumno: newAlumno});
});

router.post("/asignatura/save", async (req, res, next) => {
    var newAsignatura = await asignaturasController.save(req);
    var alumnosSelected = await asignaturasController.getAlumnosByID(req);
    res.render("asignatura-list.pug", {asignatura: newAsignatura}, {alumnos: alumnosSelected});
});

router.get("/alumnos/new", async (req, res, next) => {
    res.render("alumnos-new.pug");
});

router.post("/alumnos/created", async (req, res, next) => {
    var newAlumno = await alumnosController.save(req);
    res.render("alumnos-save.pug", {alumno: newAlumno});
});

module.exports = router;