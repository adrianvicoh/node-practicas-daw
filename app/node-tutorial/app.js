const port = 3000;
var express = require("express"),
  app = express(),
  server = require("http").createServer(app),
  path = require("path");
  body_parser = require("body-parser"),
  http = require('http'),
  mongoose = require('mongoose');

mongoose.connect(
  `mongodb://root:pass12345@mongodb:27017/tutorial?authSource=admin`,
  { useCreateIndex: true, useUnifiedTopology: true, useNewUrlParser: true },
  (err, res) => {
    if (err) console.log(`ERROR: connecting to Database.  ${err}`);
    else console.log(`Database Online`);
  }
);

server.listen(port, (err, res) => {
  if (err) console.log(`ERROR: Connecting APP ${err}`);
  else console.log(`Server is running on port ${port}`);
});

// Import routes of our app
var routes = require("./routes/main.js");

// view engine setup and other configurations
app.set("views", path.join(__dirname,"views"));
app.set("view engine", "pug");
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, "public")));

// Define routes using URL path
app.use("/", routes);
module.exports = app;
