var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AlumnoSchema = new Schema({
    name: {type: String, required: true},
    surname: {type: String, required: true},
    age: {type: String, requiserd: true},
});

module.exports = mongoose.model('Alumno', AlumnoSchema);