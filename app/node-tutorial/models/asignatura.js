var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AsignaturaSchema = new Schema({
    name: {type: String, required: true},
    alumnos: {type: Array, required: true},
});

module.exports = mongoose.model('Asignatura', AsignaturaSchema);