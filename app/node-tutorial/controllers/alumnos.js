var Alumno = require("../models/alumno");

exports.getAllAlumnos = async () => {
    try {
        return await Alumno.find(function(err, data) {
            return data;
        });
    } catch (error) {
        console.error(`Error getting all "Alumnos" ${error}`);
    }
};

exports.showAlumno = async (req) => {
    try {
        return await Alumno.findOne({_id: req.params.id}, function(error, data) {
            return data;
        });
    } catch (error) {
        console.error(`Error showing all "Alumnos" ${error}`);
    }
};

exports.save = async (req) => {
    try {
        var alumno = new Alumno({name: req.body.nombre, surname: req.body.apellido, age: req.body.edad});
        alumno.save();
        return alumno;
    } catch (error) {
        console.error(`Error saving Alumno ${error}`);
    }
};