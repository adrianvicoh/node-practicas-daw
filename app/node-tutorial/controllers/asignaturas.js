var Asignatura = require("../models/asignatura");
var Alumno = require("../models/alumno");

exports.getAllAsignaturas = async () => {
    try {
        return await Asignatura.find(function(err, data) {
            return data;
        });
    } catch (error) {
        console.error(`Error getting all "Asignatura" ${error}`);
    }
};

exports.showAsignatura = async (req) => {
    try {
        return await Asignatura.findOne({_id: req.params.id}, function(error, data) {
            return data;
        });
    } catch (error) {
        console.error(`Error showing all "Asignaturas" ${error}`);
    }
};

exports.save = async (req) => {
    try {
        var asignatura = new Asignatura({name: req.body.nombre, alumnos: req.body.alumnos});
        asignatura.save();
        return asignatura;
    } catch (error) {
        console.error(`Error saving Asignatura ${error}`);
    }
};

exports.getAlumnosByID = async (req) => {
    try {
        var alumnos = [];
        for(id of req.body.alumnos) {
            var newAlumno = await Alumno.findOne({_id: id}, function(error, data) {
                return data;
            });
            alumnos.push(newAlumno);
        }
        return alumnos;
    } catch (error) {
        console.error(`Error getting Alumno by ID ${error}`);
    }
};