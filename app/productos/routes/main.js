var express = require("express");
var path = require("path");
var router = express.Router();
var controllerDir = "../controllers";
var productosController = require(path.join(controllerDir, "productos"));

router.get("/productos", async (req, res, next) => {
    res.render("productos-number.pug");
});

router.post("/productos/create", async (req, res, next) => {
    res.render("productos-create.pug", {productsNumber: req.body.number});
});

router.post("/productos/view", async (req, res, next) => {
    var newProducts = await productosController.save(req);
    var stock = await productosController.getStock();
    res.render("productos-view.pug", {stock: stock, length: stock.length});
});

module.exports = router;