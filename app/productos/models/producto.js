var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ProductoSchema = new Schema({
    name: {type: String, required: true},
    price: {type: String, required: true},
});

module.exports = mongoose.model('Producto', ProductoSchema);