var Producto = require("../models/producto");

exports.getStock = async () => {
    try {
        return await Producto.find(function(err, data) {
            return data;
        });
    } catch (error) {
        console.error(`Error getting all "Productos" ${error}`);
    }
};

exports.save = async (req) => {
    try {
        let length = req.body.name.length;
        for(i = 0; i<length; i++) {
            var producto = new Producto({name: req.body.name[i], price: req.body.price[i]});
            producto.save();
        }
    } catch (error) {
        console.error(`Error saving Producto ${error}`);
    }
};