var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var JornadaSchema = new Schema({
    matches: {type: Array, required: true},
});

module.exports = mongoose.model('Jugador', JornadaSchema);