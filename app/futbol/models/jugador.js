var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var JugadorSchema = new Schema({
    name: {type: String, required: true},
    //matches: {type: Array, required: true},
    scores: {type: Array, required: true}
});

module.exports = mongoose.model('Jugador', JugadorSchema);