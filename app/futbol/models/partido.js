var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PartidoSchema = new Schema({
    players: {type: Array, required: true}
});

module.exports = mongoose.model('Jugador', PartidoSchema);