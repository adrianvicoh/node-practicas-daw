var express = require("express");
var path = require("path");
var router = express.Router();
var controllerDir = "../controllers";
var jugadoresController = require(path.join(controllerDir, "jugadores"));
//var partidosController = require(path.join(controllerDir, "partidos"));
//var jornadasController = require(path.join(controllerDir, "jornadas"));

router.get("/futbol", async (req, res, next) => {
    res.render("futbol-number.pug");
});

router.post("/futbol/create", async (req, res, next) => {
    res.render("futbol-create.pug", {numMatches: req.body.matches, numPlayers: req.body.players});
});

router.post("/futbol/view", async (req, res, next) => {
    jugadoresController.save(req);
    var jugadores = await jugadoresController.getAllJugadores();
    console.log(jugadores);
    var numPlayers = jugadores.length;
    var numMatches = jugadores[0].scores.length;
    res.render("futbol-view.pug", {jugadores: jugadores, numPlayers: numPlayers, numMatches: numMatches});
});

module.exports = router;