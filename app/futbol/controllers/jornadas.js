var Partido = require("../models/jornada");

exports.getAllJornadas = async () => {
    try {
        return await Jornada.find(function(err, data) {
            return data;
        });
    } catch (error) {
        console.error(`Error getting all "Jornadas" ${error}`);
    }
};

exports.showJornada = async (req) => {
    try {
        return await Jornada.findOne({_id: req.params.id}, function(error, data) {
            return data;
        });
    } catch (error) {
        console.error(`Error showing all "Jornadas" ${error}`);
    }
};

exports.save = async (req) => {
    try {
        var jornada = new Jornada({name: req.body.nombre});
        jornada.save();
        return jornada;
    } catch (error) {
        console.error(`Error saving Jornada ${error}`);
    }
};