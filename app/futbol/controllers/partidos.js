var Partido = require("../models/partido");

exports.getAllPartidos = async () => {
    try {
        return await Partido.find(function(err, data) {
            return data;
        });
    } catch (error) {
        console.error(`Error getting all "Partidos" ${error}`);
    }
};

exports.showPartido = async (req) => {
    try {
        return await Partido.findOne({_id: req.params.id}, function(error, data) {
            return data;
        });
    } catch (error) {
        console.error(`Error showing all "Partidos" ${error}`);
    }
};

exports.save = async (req) => {
    try {
        var partido = new Partido({name: req.body.nombre});
        partido.save();
        return partido;
    } catch (error) {
        console.error(`Error saving Partido ${error}`);
    }
};