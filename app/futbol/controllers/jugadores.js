var Jugador = require("../models/jugador");

exports.getAllJugadores = async () => {
    try {
        return await Jugador.find(function(err, data) {
            return data;
        });
    } catch (error) {
        console.error(`Error getting all "Jugadores" ${error}`);
    }
};

exports.save = async (req) => {
    try {
        let scoresLength = req.body.scores.length;
        let playersLength = req.body.players.length;
        for(i=0;i<playersLength;i++) {
            let newScores = [];
            for(j=0;j<playersLength;j++) {
                newScores.push(req.body.scores[i][j]);
            }
            let jugador = new Jugador({name: req.body.players[i], scores: newScores});
            jugador.save();
            console.log(jugador);
        }
    } catch (error) {
        console.error(`Error saving Jugador ${error}`);
    }
};