const port = 3000;
var express = require("express"),
  app = express(),
  server = require("http").createServer(app),
  path = require("path");
  body_parser = require("body-parser"),
  http = require('http'),
  mongoose = require('mongoose'),
  session = require("express-session");

mongoose.connect(
  `mongodb://root:pass12345@mongodb:27017/tutorial?authSource=admin`,
  { useCreateIndex: true, useUnifiedTopology: true, useNewUrlParser: true },
  (err, res) => {
    if (err) console.log(`ERROR: connecting to Database.  ${err}`);
    else console.log(`Database Online`);
  }
);

app.use(session({
  secret: "987f4bd6d4315c20b2ec70a46ae846d19d0ce563450c02c5b1bc71d5d580060b",
  saveUninitialized: true,
  resave: true,
}));

server.listen(port, (err, res) => {
  if (err) console.log(`ERROR: Connecting APP ${err}`);
  else console.log(`Server is running on port ${port}`);
});

const { nextTick } = require("process");
// Import routes of our app
var routes = require("./routes/main.js");

// view engine setup and other configurations
app.set("views", path.join(__dirname,"views"));
app.set("view engine", "pug");
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, "public")));

var isAuth = function (req, res, next) {
  if(req.session.isAuth == true && req.session.info.rol == 'admin') {
    next();
  } else {
    //alert("No estás autenticado");
    //res.redirect("/info");
    res.send("No estás autenticado. Inicia sesión como admin");
  }
};

// Define routes using URL path
app.use("/admin", isAuth);
app.use("/", routes);
module.exports = app;