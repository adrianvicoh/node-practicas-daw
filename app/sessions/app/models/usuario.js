var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UsuarioSchema = new Schema({
    name: {type: String, required: true},
    password: {type: String, required: true},
    rol: {type: String, required: true},
    //timestamp: Date.now()
});

module.exports = mongoose.model('Usuario', UsuarioSchema);