var Usuario = require("../models/usuario");

exports.createUser = async (req) => {
    try {
        var usuario = new Usuario({name: req.body.user, password: req.body.password, rol: req.body.rol});
        usuario.save();
        return usuario;
    } catch (error) {
        console.error(`Error saving Alumno ${error}`);
    }
};