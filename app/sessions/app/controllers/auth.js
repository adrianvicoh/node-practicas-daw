var Usuario = require("../models/usuario");

exports.autenticar = async (req) => {
    try {
        let usuarioInput = req.body.user;
        let passwordInput = req.body.password;
        let auth = false;
        let usuarioSearch = await Usuario.findOne({name: usuarioInput}, function(error, data) {
            return data;
        });
        if(usuarioSearch != null) {
            if(usuarioSearch.password == passwordInput) {
                console.log(req.session.isAuth);
                req.session.isAuth = true;
                console.log(req.session.isAuth);
                console.log(req.session.info);
                req.session.info = {name: usuarioInput, rol: usuarioSearch.rol};
                console.log(req.session.info);
                auth = true;
            }
        }
        return auth;
    } catch (error) {
        console.error(`Error authentication ${error}`);
    }
};