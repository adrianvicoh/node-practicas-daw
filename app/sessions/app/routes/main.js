var express = require("express");
const { appendFile } = require("fs");
var path = require("path");
var router = express.Router();
var app = express();
var controllerDir = "../controllers";
var authController = require(path.join(controllerDir, "auth"));
var usuarioController = require(path.join(controllerDir, "usuarios"));

router.get("/", async (req, res, next) => {
    res.render("login-page");
});


router.get('/info', (req, res) => {
    res.render('info');
});

router.get('/new', (req, res) => {
    res.render("signup-page");
});

router.post('/create', (req, res) => {
    var newUsuario = usuarioController.createUser(req);
    res.redirect('/');
});

router.post('/auth', async (req, res, next) => {
    validation = await authController.autenticar(req);
    res.redirect('/info');
});

/*Access only if logged in with session */
router.get("/admin/welcome", async (req, res, next) => {
    res.render("/admin-welcome");
});
router.get("/admin/info", async (req, res, next) => {
    res.render("admin-info");
});

module.exports = router;
